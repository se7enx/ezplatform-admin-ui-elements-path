(function(global) {
    if (CKEDITOR.plugins.get('elements-path')) {
        return;
    }

    const install = function(editor) {
        const wrapper = new CKEDITOR.dom.element('div');
        wrapper.addClass('elements-path-wrapper');
        editor.container.getParent().append(wrapper);

        const elementsPath = new CKEDITOR.dom.element('ul');
        wrapper.append(elementsPath);

        editor.elementsPathContainer = elementsPath;
    };
    const updateCommand = {
        elements: [],
        editor: null,
        itemTemplate: new CKEDITOR.template('<a href="#">{label}</a>'),
        cssClasses: {
            customTagWrapper: 'cke_widget_wrapper_ez-custom-tag',
            embedWrapper: 'cke_widget_ezembed',
            embedImage: 'ez-embed-type-image',
            customTagContentVisible: 'ez-custom-tag--content-visible'
        },
        attributes: {
            eZName: 'data-ezname',
            eZElement: 'data-ezelement',
            eZType: 'data-eztype',
            widgetName: 'data-widget'
        },
        labels: {
            customTag: 'Custom Tag',
            embed: 'Embed',
            image: 'Image',
            customStyle: 'Custom Style',
            p: 'Paragraph',
            h1: 'Heading 1',
            h2: 'Heading 2',
            h3: 'Heading 3',
            h4: 'Heading 4',
            h5: 'Heading 5',
            h6: 'Heading 6',
            pre: 'Formatted',
            ul: 'Unordered List',
            ol: 'Ordered List',
            li: 'List Item',
            table: 'Table',
            tr: 'Row',
            th: 'Header',
            td: 'Column',
            strong: 'Strong',
            em: 'Emphasized',
            u: 'Underlined',
            sub: 'Subscript',
            sup: 'Superscript',
            blockquote: 'Quoted',
            s: 'Strikethrough',
            a: 'Link'
        },
        skipElements: [
            'thead',
            'tbody'
        ],
        exec: function(editor, path) {
            this.editor = editor;
            this.editor.elementsPathContainer.setHtml('');

            // Reverse path and skip first item, which is editor container
            this.elements = path.reverse().slice(1);

            // Get output items and build path for them
            this.getOutputPathElements().forEach(function (outputItem) {
                this.editor.elementsPathContainer.append(outputItem.pathItem);
            }, this);
        },
        getOutputPathElements: function() {
            let outputPathElements = [];
            const length = this.elements.length;

            for (let i = 0; i < length; i++) {
                let element = this.elements[i];
                if (element !== null && this.skipElements.indexOf(element.getName()) === -1) {
                    const outputPathElement = this.getOutputPathItem(element, i);
                    if (typeof outputPathElement.pathItem !== 'undefined') {
                        outputPathElements.push(outputPathElement);
                    }
                }
            }

            return outputPathElements;
        },
        getOutputPathItem: function(element, index) {
            let outputItem = {};
            let elementName = element.getName();
            let label = elementName;

            if (this.isBlockCustomTag(element) || this.isInlineCustomTag(element)) {
                outputItem.originalElement = element;
                label = this.getCustomTagPathLabel(element);
                this.fixElementsForCustomTag(element, index);
            } else if (this.isEmbedImage(element)) {
                label = this.labels.image;
            } else if (this.isEmbed(element)) {
                label = this.labels.embed;
            } else if (this.isBlockCustomStyle(element) || this.isInlineCustomStyle(element)) {
                label = this.getCustomStylePathLabel(element);
            }

            if (typeof this.labels[elementName] !== 'undefined') {
                label = this.labels[elementName];
            }

            outputItem.pathItem = this.getPathItem(element, label);
            return outputItem;
        },
        isCustomTag: function(element) {
            return element.hasClass(this.cssClasses.customTagWrapper);
        },
        isBlockCustomTag: function(element) {
            if (this.isCustomTag(element) === false) {
                return false;
            }

            let list = element.find('[' + this.attributes.eZElement + ']');
            if (list.count() === 0) {
                return false;
            }

            return list.getItem(0).getAttribute(this.attributes.widgetName) === 'ezcustomtag';
        },
        isInlineCustomTag: function(element) {
            if (this.isCustomTag(element) === false) {
                return false;
            }

            let list = element.find('[' + this.attributes.eZElement + ']');
            if (list.count() === 0) {
                return false;
            }

            return list.getItem(0).getAttribute(this.attributes.widgetName) === 'ezinlinecustomtag';
        },
        isEmbed: function(element) {
            return element.hasClass(this.cssClasses.embedWrapper);
        },
        isEmbedImage: function(element) {
            if (this.isEmbed(element) === false) {
                return false;
            }

            let list = element.find('[' + this.attributes.eZElement + ']');
            if (list.count() === 0) {
                return false;
            }

            return list.getItem(0).hasClass(this.cssClasses.embedImage);
        },
        isCustomStyle: function(element) {
            return element.getAttribute(this.attributes.eZType) === 'style';
        },
        isBlockCustomStyle: function(element) {
            if (this.isCustomStyle(element) === false) {
                return false;
            }
            return element.getName() === 'div';
        },
        isInlineCustomStyle: function(element) {
            if (this.isCustomStyle(element) === false) {
                return false;
            }
            return element.getName() === 'span';
        },
        getCustomTagPathLabel: function(element) {
            let label = this.labels.customTag;
            let list = element.find('[' + this.attributes.eZName + ']');
            if (list.count() > 0) {
                label += ' "' + list.getItem(0).getAttribute(this.attributes.eZName) + '"';
            }
            return label;
        },
        getCustomStylePathLabel: function(element) {
            let label = this.labels.customStyle;
            if (element.hasAttribute(this.attributes.eZName)) {
                label += ' "' + element.getAttribute(this.attributes.eZName) + '"';
            }
            return label;
        },
        fixElementsForCustomTag: function(element, index) {
            // When custom tag value is focused, there are few extra divs in the path
            // And we need to remove them.
            let indexToCheck = index + 1;
            if (typeof this.elements[indexToCheck] === 'undefined') {
                return;
            }
            if (this.elements[indexToCheck].hasAttribute(this.attributes.eZName)) {
                this.elements[indexToCheck] = null;
            }

            indexToCheck = index + 2;
            if (typeof this.elements[indexToCheck] === 'undefined') {
                return;
            }
            if (this.elements[indexToCheck].hasAttribute(this.attributes.eZElement)) {
                this.elements[indexToCheck] = null;
            }
        },
        getPathItem: function(element, label) {
            let item = CKEDITOR.dom.element.createFromHtml(this.itemTemplate.output({label}));

            let that = this;
            item.on('click', function (event) {
                event.data.preventDefault();

                // Do not do nothing for embed and image elements
                if (that.isEmbed(this.element)) {
                    return;
                }

                // For custom tags we need to switch element it its content paragraph
                if (that.isCustomTag(this.element)) {
                    // We do nothing if custom tag content is not shown
                    if (this.element.getChild(0).hasClass(that.cssClasses.customTagContentVisible) === false) {
                        return;
                    }

                    let list = this.element.find('[data-ezelement="ezcontent"]>p');
                    if (list.count() > 0) {
                        this.element = list.getItem(0);
                    }
                }

                // Use first child for quote
                if (this.element.getName() === 'blockquote') {
                    this.element = this.element.getChild(0);
                }

                let range = this.editor.createRange();
                range.moveToPosition(this.element, CKEDITOR.POSITION_AFTER_START);
                this.editor.getSelection().selectRanges([range]);
                this.editor.fire('editorInteraction', {
                    nativeEvent: {
                        editor: this.editor,
                        target: this.element.$,
                    },
                    selectionData: this.editor.getSelectionData()
                });
            }, {element: element, editor: this.editor});

            let listItem = new CKEDITOR.dom.element('li');
            listItem.append(item);

            return listItem;
        }
    };

    CKEDITOR.plugins.add('elements-path', {
        init: (editor) => {
            editor.on('contentDom', (event) => {
                install(event.editor);
            });
            editor.addCommand('update-elements-path', updateCommand);
            editor.on('selectionChange', (event) => {
                event.editor.execCommand('update-elements-path', event.data.path.elements);
            });
        },
    });
})(window);
