# eZ Platform Admin UI Elements Path Bundle

Extends eZ Platform Online Editor by adding Elements Path block. 

## Installation

1. Run composer require:
    ```bash
    composer require contextualcode/ezplatform-admin-ui-elements-path
    ```

2. Enable bundle in `app/AppKernel.php`:
    ```php
    new ContextualCode\EzPlatformAdminUiElementsPathBundle\EzPlatformAdminUiElementsPathBundle()
    ```

3. Clear cache to update `encore` configurations:
    ```bash
    php bin/console cache:clear
    ```

4. Install assets:
    ```bash
    php bin/console assets:install public --symlink --relative
    ```
    
5. Update assets:
    ```bash
    yarn encore dev
    ```
    
## Usage

Try to edit any content which has `RichText` field. And there should be Elements Path block in each instance of eZ Platform Online Editor.  